# Ensi\CrmClient\ProductSubscribesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clearProductSubscribe**](ProductSubscribesApi.md#clearProductSubscribe) | **POST** /customers/product-subscribes:clear | Удаление всех товаров из подписок клиента
[**createProductSubscribe**](ProductSubscribesApi.md#createProductSubscribe) | **POST** /customers/product-subscribes | Создание объекта типа ProductSubscribe
[**deleteProductSubscribe**](ProductSubscribesApi.md#deleteProductSubscribe) | **POST** /customers/product-subscribes:delete-product | Удаление товара из списка подписок клиента
[**searchProductSubscribe**](ProductSubscribesApi.md#searchProductSubscribe) | **POST** /customers/product-subscribes:search | Поиск объектов типа ProductSubscribe



## clearProductSubscribe

> \Ensi\CrmClient\Dto\EmptyDataResponse clearProductSubscribe($clear_product_subscribes_request)

Удаление всех товаров из подписок клиента

Удаление всех товаров из подписок клиента

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\ProductSubscribesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clear_product_subscribes_request = new \Ensi\CrmClient\Dto\ClearProductSubscribesRequest(); // \Ensi\CrmClient\Dto\ClearProductSubscribesRequest | 

try {
    $result = $apiInstance->clearProductSubscribe($clear_product_subscribes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductSubscribesApi->clearProductSubscribe: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clear_product_subscribes_request** | [**\Ensi\CrmClient\Dto\ClearProductSubscribesRequest**](../Model/ClearProductSubscribesRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createProductSubscribe

> \Ensi\CrmClient\Dto\ProductSubscribeResponse createProductSubscribe($product_subscribe_for_create)

Создание объекта типа ProductSubscribe

Создание объекта типа ProductSubscribe

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\ProductSubscribesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$product_subscribe_for_create = new \Ensi\CrmClient\Dto\ProductSubscribeForCreate(); // \Ensi\CrmClient\Dto\ProductSubscribeForCreate | 

try {
    $result = $apiInstance->createProductSubscribe($product_subscribe_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductSubscribesApi->createProductSubscribe: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_subscribe_for_create** | [**\Ensi\CrmClient\Dto\ProductSubscribeForCreate**](../Model/ProductSubscribeForCreate.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\ProductSubscribeResponse**](../Model/ProductSubscribeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProductSubscribe

> \Ensi\CrmClient\Dto\EmptyDataResponse deleteProductSubscribe($delete_product_subscribes_request)

Удаление товара из списка подписок клиента

Удаление товара из списка подписок клиента

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\ProductSubscribesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$delete_product_subscribes_request = new \Ensi\CrmClient\Dto\DeleteProductSubscribesRequest(); // \Ensi\CrmClient\Dto\DeleteProductSubscribesRequest | 

try {
    $result = $apiInstance->deleteProductSubscribe($delete_product_subscribes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductSubscribesApi->deleteProductSubscribe: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_product_subscribes_request** | [**\Ensi\CrmClient\Dto\DeleteProductSubscribesRequest**](../Model/DeleteProductSubscribesRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductSubscribe

> \Ensi\CrmClient\Dto\SearchProductSubscribesResponse searchProductSubscribe($search_product_subscribes_request)

Поиск объектов типа ProductSubscribe

Поиск объектов типа ProductSubscribe

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\ProductSubscribesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_subscribes_request = new \Ensi\CrmClient\Dto\SearchProductSubscribesRequest(); // \Ensi\CrmClient\Dto\SearchProductSubscribesRequest | 

try {
    $result = $apiInstance->searchProductSubscribe($search_product_subscribes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductSubscribesApi->searchProductSubscribe: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_subscribes_request** | [**\Ensi\CrmClient\Dto\SearchProductSubscribesRequest**](../Model/SearchProductSubscribesRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\SearchProductSubscribesResponse**](../Model/SearchProductSubscribesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

