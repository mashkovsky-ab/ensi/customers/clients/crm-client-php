# Ensi\CrmClient\CustomerFavoritesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clearCustomerFavorites**](CustomerFavoritesApi.md#clearCustomerFavorites) | **POST** /customers/favorites:clear | Удаление всех товаров из списка избранного клиента
[**createCustomerFavorite**](CustomerFavoritesApi.md#createCustomerFavorite) | **POST** /customers/favorites | Создание объекта типа CustomerFavorite
[**deleteProductFromCustomerFavorites**](CustomerFavoritesApi.md#deleteProductFromCustomerFavorites) | **POST** /customers/favorites:delete-product | Удаление товара из списка избранного клиента
[**searchCustomerFavorite**](CustomerFavoritesApi.md#searchCustomerFavorite) | **POST** /customers/favorites:search | Поиск объектов типа CustomerFavorite



## clearCustomerFavorites

> \Ensi\CrmClient\Dto\EmptyDataResponse clearCustomerFavorites($clear_customer_favorites_request)

Удаление всех товаров из списка избранного клиента

Удаление всех товаров из списка избранного клиента

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\CustomerFavoritesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clear_customer_favorites_request = new \Ensi\CrmClient\Dto\ClearCustomerFavoritesRequest(); // \Ensi\CrmClient\Dto\ClearCustomerFavoritesRequest | 

try {
    $result = $apiInstance->clearCustomerFavorites($clear_customer_favorites_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerFavoritesApi->clearCustomerFavorites: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clear_customer_favorites_request** | [**\Ensi\CrmClient\Dto\ClearCustomerFavoritesRequest**](../Model/ClearCustomerFavoritesRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createCustomerFavorite

> \Ensi\CrmClient\Dto\CustomerFavoriteResponse createCustomerFavorite($customer_favorite_for_create)

Создание объекта типа CustomerFavorite

Создание объекта типа CustomerFavorite

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\CustomerFavoritesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$customer_favorite_for_create = new \Ensi\CrmClient\Dto\CustomerFavoriteForCreate(); // \Ensi\CrmClient\Dto\CustomerFavoriteForCreate | 

try {
    $result = $apiInstance->createCustomerFavorite($customer_favorite_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerFavoritesApi->createCustomerFavorite: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_favorite_for_create** | [**\Ensi\CrmClient\Dto\CustomerFavoriteForCreate**](../Model/CustomerFavoriteForCreate.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\CustomerFavoriteResponse**](../Model/CustomerFavoriteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProductFromCustomerFavorites

> \Ensi\CrmClient\Dto\EmptyDataResponse deleteProductFromCustomerFavorites($delete_product_from_customer_favorites_request)

Удаление товара из списка избранного клиента

Удаление товара из списка избранного клиента

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\CustomerFavoritesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$delete_product_from_customer_favorites_request = new \Ensi\CrmClient\Dto\DeleteProductFromCustomerFavoritesRequest(); // \Ensi\CrmClient\Dto\DeleteProductFromCustomerFavoritesRequest | 

try {
    $result = $apiInstance->deleteProductFromCustomerFavorites($delete_product_from_customer_favorites_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerFavoritesApi->deleteProductFromCustomerFavorites: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_product_from_customer_favorites_request** | [**\Ensi\CrmClient\Dto\DeleteProductFromCustomerFavoritesRequest**](../Model/DeleteProductFromCustomerFavoritesRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchCustomerFavorite

> \Ensi\CrmClient\Dto\SearchCustomerFavoritesResponse searchCustomerFavorite($search_customer_favorites_request)

Поиск объектов типа CustomerFavorite

Поиск объектов типа CustomerFavorite

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\CustomerFavoritesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_customer_favorites_request = new \Ensi\CrmClient\Dto\SearchCustomerFavoritesRequest(); // \Ensi\CrmClient\Dto\SearchCustomerFavoritesRequest | 

try {
    $result = $apiInstance->searchCustomerFavorite($search_customer_favorites_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerFavoritesApi->searchCustomerFavorite: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_customer_favorites_request** | [**\Ensi\CrmClient\Dto\SearchCustomerFavoritesRequest**](../Model/SearchCustomerFavoritesRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\SearchCustomerFavoritesResponse**](../Model/SearchCustomerFavoritesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

