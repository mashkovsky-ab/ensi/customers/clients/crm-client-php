# Ensi\CrmClient\PreferencesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchPreferences**](PreferencesApi.md#searchPreferences) | **POST** /customers/preferences:search | Поиск объектов типа Preferences



## searchPreferences

> \Ensi\CrmClient\Dto\SearchPreferencesResponse searchPreferences($search_preferences_request)

Поиск объектов типа Preferences

Поиск объектов типа Preferences

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\PreferencesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_preferences_request = new \Ensi\CrmClient\Dto\SearchPreferencesRequest(); // \Ensi\CrmClient\Dto\SearchPreferencesRequest | 

try {
    $result = $apiInstance->searchPreferences($search_preferences_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PreferencesApi->searchPreferences: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_preferences_request** | [**\Ensi\CrmClient\Dto\SearchPreferencesRequest**](../Model/SearchPreferencesRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\SearchPreferencesResponse**](../Model/SearchPreferencesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

