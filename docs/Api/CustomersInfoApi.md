# Ensi\CrmClient\CustomersInfoApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCustomerInfo**](CustomersInfoApi.md#createCustomerInfo) | **POST** /customers/customers-info | Создание объекта типа CustomerInfo
[**deleteCustomerInfo**](CustomersInfoApi.md#deleteCustomerInfo) | **DELETE** /customers/customers-info/{id} | Удаление объекта типа CustomerInfo
[**getCustomerInfo**](CustomersInfoApi.md#getCustomerInfo) | **GET** /customers/customers-info/{id} | Получение объекта типа CustomerInfo
[**patchCustomerInfo**](CustomersInfoApi.md#patchCustomerInfo) | **PATCH** /customers/customers-info/{id} | Обновления части полей объекта типа CustomerInfo
[**searchCustomersInfo**](CustomersInfoApi.md#searchCustomersInfo) | **POST** /customers/customers-info:search | Поиск объектов типа CustomerInfo



## createCustomerInfo

> \Ensi\CrmClient\Dto\CustomerInfoResponse createCustomerInfo($customer_info_for_create)

Создание объекта типа CustomerInfo

Создание объекта типа CustomerInfo

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\CustomersInfoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$customer_info_for_create = new \Ensi\CrmClient\Dto\CustomerInfoForCreate(); // \Ensi\CrmClient\Dto\CustomerInfoForCreate | 

try {
    $result = $apiInstance->createCustomerInfo($customer_info_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersInfoApi->createCustomerInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_info_for_create** | [**\Ensi\CrmClient\Dto\CustomerInfoForCreate**](../Model/CustomerInfoForCreate.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\CustomerInfoResponse**](../Model/CustomerInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteCustomerInfo

> \Ensi\CrmClient\Dto\EmptyDataResponse deleteCustomerInfo($id)

Удаление объекта типа CustomerInfo

Удаление объекта типа CustomerInfo

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\CustomersInfoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteCustomerInfo($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersInfoApi->deleteCustomerInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CrmClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getCustomerInfo

> \Ensi\CrmClient\Dto\CustomerInfoResponse getCustomerInfo($id, $include)

Получение объекта типа CustomerInfo

Получение объекта типа CustomerInfo

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\CustomersInfoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getCustomerInfo($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersInfoApi->getCustomerInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\CrmClient\Dto\CustomerInfoResponse**](../Model/CustomerInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchCustomerInfo

> \Ensi\CrmClient\Dto\CustomerInfoResponse patchCustomerInfo($id, $customer_info_for_patch)

Обновления части полей объекта типа CustomerInfo

Обновления части полей объекта типа CustomerInfo

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\CustomersInfoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$customer_info_for_patch = new \Ensi\CrmClient\Dto\CustomerInfoForPatch(); // \Ensi\CrmClient\Dto\CustomerInfoForPatch | 

try {
    $result = $apiInstance->patchCustomerInfo($id, $customer_info_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersInfoApi->patchCustomerInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **customer_info_for_patch** | [**\Ensi\CrmClient\Dto\CustomerInfoForPatch**](../Model/CustomerInfoForPatch.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\CustomerInfoResponse**](../Model/CustomerInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchCustomersInfo

> \Ensi\CrmClient\Dto\SearchCustomersInfoResponse searchCustomersInfo($search_customers_info_request)

Поиск объектов типа CustomerInfo

Поиск объектов типа CustomerInfo

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\CustomersInfoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_customers_info_request = new \Ensi\CrmClient\Dto\SearchCustomersInfoRequest(); // \Ensi\CrmClient\Dto\SearchCustomersInfoRequest | 

try {
    $result = $apiInstance->searchCustomersInfo($search_customers_info_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersInfoApi->searchCustomersInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_customers_info_request** | [**\Ensi\CrmClient\Dto\SearchCustomersInfoRequest**](../Model/SearchCustomersInfoRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\SearchCustomersInfoResponse**](../Model/SearchCustomersInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

