# # BonusOperationForCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_id** | **int** | Идентификатор клиента | [optional] 
**order_number** | **int** | Номре заказа | [optional] 
**bonus_amount** | **float** | Количество начисления/списания бонусов (с “-” при списании) | [optional] 
**comment** | **string** | Комментарий к бонусной операции | [optional] 
**activation_date** | **string** | Дата активации | [optional] 
**expiration_date** | **string** | Дата сгорания | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


