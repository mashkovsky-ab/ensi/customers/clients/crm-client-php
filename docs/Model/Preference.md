# # Preference

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор записи | [optional] 
**customer_id** | **int** | Идентификатор клиента | [optional] 
**attribute_name** | **string** | Название атрибута | [optional] 
**attribute_value** | **string** | Значение атрибута атрибута | [optional] 
**product_count** | **int** | Колличество купленных товаров | [optional] 
**product_sum** | **int** | Cумма стоимости купленных товаров | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


