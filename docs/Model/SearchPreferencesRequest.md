# # SearchPreferencesRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sort** | **string[]** |  | [optional] 
**filter** | [**\Ensi\CrmClient\Dto\SearchPreferencesFilter**](SearchPreferencesFilter.md) |  | [optional] 
**include** | **string[]** |  | [optional] 
**pagination** | [**\Ensi\CrmClient\Dto\RequestBodyPagination**](RequestBodyPagination.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


