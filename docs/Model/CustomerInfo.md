# # CustomerInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор покупателя | [optional] 
**avatar** | [**\Ensi\CrmClient\Dto\File**](File.md) |  | [optional] 
**customer_id** | **int** | Идентификатор пользователя | [optional] 
**kpi_sku_count** | **int** | Колличетсво купленных товаров | [optional] 
**kpi_sku_price_sum** | **int** | Сумма цен купленных товаров | [optional] 
**kpi_order_count** | **int** | Колличетсво офрмленных заказов | [optional] 
**kpi_shipment_count** | **int** | Колличетсво оформленных отправок | [optional] 
**kpi_delivered_count** | **int** | Колличетсво досталенных | [optional] 
**kpi_delivered_sum** | **int** | Сумма цен доставленных | [optional] 
**kpi_refunded_count** | **int** | Колличетсво возвращенных | [optional] 
**kpi_refunded_sum** | **int** | Сумма цен возвращенных | [optional] 
**kpi_canceled_count** | **int** | Колличетсво отмененных | [optional] 
**kpi_canceled_sum** | **int** | Сумма цен отмененных | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


