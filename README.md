# CrmClient

Управление клиентами

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 1.0.0
- Build package: org.openapitools.codegen.languages.PhpClientCodegen
For more information, please visit [https://ensi.tech/contacts](https://ensi.tech/contacts)

## Requirements

PHP 5.5 and later

## Installation & Usage

### Composer

To install the bindings via [Composer](http://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/greensight/ensi/customers/clients/crm-client-php.git"
    }
  ],
  "require": {
    "greensight/ensi/customers/clients/crm-client-php": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
    require_once('/path/to/CrmClient/vendor/autoload.php');
```

## Tests

To run the unit tests:

```bash
composer install
./vendor/bin/phpunit
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new Ensi\CrmClient\Api\BonusOperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$bonus_operation_for_create = new \Ensi\CrmClient\Dto\BonusOperationForCreate(); // \Ensi\CrmClient\Dto\BonusOperationForCreate | 

try {
    $result = $apiInstance->createBonusOperation($bonus_operation_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BonusOperationsApi->createBonusOperation: ', $e->getMessage(), PHP_EOL;
}

?>
```

## Documentation for API Endpoints

All URIs are relative to *http://localhost/api/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*BonusOperationsApi* | [**createBonusOperation**](docs/Api/BonusOperationsApi.md#createbonusoperation) | **POST** /customers/bonus-operations | Создание бонусной операции
*BonusOperationsApi* | [**deleteBonusOperation**](docs/Api/BonusOperationsApi.md#deletebonusoperation) | **DELETE** /customers/bonus-operations/{id} | Удаление бонусной операции
*BonusOperationsApi* | [**getBonusOperation**](docs/Api/BonusOperationsApi.md#getbonusoperation) | **GET** /customers/bonus-operations/{id} | Получение бонусной операции
*BonusOperationsApi* | [**patchBonusOperation**](docs/Api/BonusOperationsApi.md#patchbonusoperation) | **PATCH** /customers/bonus-operations/{id} | Обновление полей бонусной операции
*BonusOperationsApi* | [**searchBonusOperation**](docs/Api/BonusOperationsApi.md#searchbonusoperation) | **POST** /customers/bonus-operations:search | Поиск бонусных операций
*CustomerFavoritesApi* | [**clearCustomerFavorites**](docs/Api/CustomerFavoritesApi.md#clearcustomerfavorites) | **POST** /customers/favorites:clear | Удаление всех товаров из списка избранного клиента
*CustomerFavoritesApi* | [**createCustomerFavorite**](docs/Api/CustomerFavoritesApi.md#createcustomerfavorite) | **POST** /customers/favorites | Создание объекта типа CustomerFavorite
*CustomerFavoritesApi* | [**deleteProductFromCustomerFavorites**](docs/Api/CustomerFavoritesApi.md#deleteproductfromcustomerfavorites) | **POST** /customers/favorites:delete-product | Удаление товара из списка избранного клиента
*CustomerFavoritesApi* | [**searchCustomerFavorite**](docs/Api/CustomerFavoritesApi.md#searchcustomerfavorite) | **POST** /customers/favorites:search | Поиск объектов типа CustomerFavorite
*CustomersInfoApi* | [**createCustomerInfo**](docs/Api/CustomersInfoApi.md#createcustomerinfo) | **POST** /customers/customers-info | Создание объекта типа CustomerInfo
*CustomersInfoApi* | [**deleteCustomerInfo**](docs/Api/CustomersInfoApi.md#deletecustomerinfo) | **DELETE** /customers/customers-info/{id} | Удаление объекта типа CustomerInfo
*CustomersInfoApi* | [**getCustomerInfo**](docs/Api/CustomersInfoApi.md#getcustomerinfo) | **GET** /customers/customers-info/{id} | Получение объекта типа CustomerInfo
*CustomersInfoApi* | [**patchCustomerInfo**](docs/Api/CustomersInfoApi.md#patchcustomerinfo) | **PATCH** /customers/customers-info/{id} | Обновления части полей объекта типа CustomerInfo
*CustomersInfoApi* | [**searchCustomersInfo**](docs/Api/CustomersInfoApi.md#searchcustomersinfo) | **POST** /customers/customers-info:search | Поиск объектов типа CustomerInfo
*PreferencesApi* | [**searchPreferences**](docs/Api/PreferencesApi.md#searchpreferences) | **POST** /customers/preferences:search | Поиск объектов типа Preferences
*ProductSubscribesApi* | [**clearProductSubscribe**](docs/Api/ProductSubscribesApi.md#clearproductsubscribe) | **POST** /customers/product-subscribes:clear | Удаление всех товаров из подписок клиента
*ProductSubscribesApi* | [**createProductSubscribe**](docs/Api/ProductSubscribesApi.md#createproductsubscribe) | **POST** /customers/product-subscribes | Создание объекта типа ProductSubscribe
*ProductSubscribesApi* | [**deleteProductSubscribe**](docs/Api/ProductSubscribesApi.md#deleteproductsubscribe) | **POST** /customers/product-subscribes:delete-product | Удаление товара из списка подписок клиента
*ProductSubscribesApi* | [**searchProductSubscribe**](docs/Api/ProductSubscribesApi.md#searchproductsubscribe) | **POST** /customers/product-subscribes:search | Поиск объектов типа ProductSubscribe


## Documentation For Models

 - [BonusOperation](docs/Model/BonusOperation.md)
 - [BonusOperationFillableProperties](docs/Model/BonusOperationFillableProperties.md)
 - [BonusOperationForCreate](docs/Model/BonusOperationForCreate.md)
 - [BonusOperationForPatch](docs/Model/BonusOperationForPatch.md)
 - [BonusOperationReadonlyProperties](docs/Model/BonusOperationReadonlyProperties.md)
 - [BonusOperationResponse](docs/Model/BonusOperationResponse.md)
 - [ClearCustomerFavoritesRequest](docs/Model/ClearCustomerFavoritesRequest.md)
 - [ClearProductSubscribesRequest](docs/Model/ClearProductSubscribesRequest.md)
 - [CustomerFavorite](docs/Model/CustomerFavorite.md)
 - [CustomerFavoriteFillableProperties](docs/Model/CustomerFavoriteFillableProperties.md)
 - [CustomerFavoriteForCreate](docs/Model/CustomerFavoriteForCreate.md)
 - [CustomerFavoriteReadonlyProperties](docs/Model/CustomerFavoriteReadonlyProperties.md)
 - [CustomerFavoriteResponse](docs/Model/CustomerFavoriteResponse.md)
 - [CustomerGenderEnum](docs/Model/CustomerGenderEnum.md)
 - [CustomerInfo](docs/Model/CustomerInfo.md)
 - [CustomerInfoFillableProperties](docs/Model/CustomerInfoFillableProperties.md)
 - [CustomerInfoForCreate](docs/Model/CustomerInfoForCreate.md)
 - [CustomerInfoForPatch](docs/Model/CustomerInfoForPatch.md)
 - [CustomerInfoReadonlyProperties](docs/Model/CustomerInfoReadonlyProperties.md)
 - [CustomerInfoResponse](docs/Model/CustomerInfoResponse.md)
 - [CustomerStatusEnum](docs/Model/CustomerStatusEnum.md)
 - [DeleteProductFromCustomerFavoritesRequest](docs/Model/DeleteProductFromCustomerFavoritesRequest.md)
 - [DeleteProductSubscribesRequest](docs/Model/DeleteProductSubscribesRequest.md)
 - [EmptyDataResponse](docs/Model/EmptyDataResponse.md)
 - [Error](docs/Model/Error.md)
 - [ErrorResponse](docs/Model/ErrorResponse.md)
 - [File](docs/Model/File.md)
 - [PaginationTypeCursorEnum](docs/Model/PaginationTypeCursorEnum.md)
 - [PaginationTypeEnum](docs/Model/PaginationTypeEnum.md)
 - [PaginationTypeOffsetEnum](docs/Model/PaginationTypeOffsetEnum.md)
 - [Preference](docs/Model/Preference.md)
 - [PreferenceFillableProperties](docs/Model/PreferenceFillableProperties.md)
 - [PreferenceReadonlyProperties](docs/Model/PreferenceReadonlyProperties.md)
 - [ProductSubscribe](docs/Model/ProductSubscribe.md)
 - [ProductSubscribeFillableProperties](docs/Model/ProductSubscribeFillableProperties.md)
 - [ProductSubscribeForCreate](docs/Model/ProductSubscribeForCreate.md)
 - [ProductSubscribeReadonlyProperties](docs/Model/ProductSubscribeReadonlyProperties.md)
 - [ProductSubscribeResponse](docs/Model/ProductSubscribeResponse.md)
 - [RequestBodyCursorPagination](docs/Model/RequestBodyCursorPagination.md)
 - [RequestBodyOffsetPagination](docs/Model/RequestBodyOffsetPagination.md)
 - [RequestBodyPagination](docs/Model/RequestBodyPagination.md)
 - [ResponseBodyCursorPagination](docs/Model/ResponseBodyCursorPagination.md)
 - [ResponseBodyOffsetPagination](docs/Model/ResponseBodyOffsetPagination.md)
 - [ResponseBodyPagination](docs/Model/ResponseBodyPagination.md)
 - [SearchBonusOperationsRequest](docs/Model/SearchBonusOperationsRequest.md)
 - [SearchBonusOperationsResponse](docs/Model/SearchBonusOperationsResponse.md)
 - [SearchCustomerFavoritesRequest](docs/Model/SearchCustomerFavoritesRequest.md)
 - [SearchCustomerFavoritesResponse](docs/Model/SearchCustomerFavoritesResponse.md)
 - [SearchCustomersInfoFilter](docs/Model/SearchCustomersInfoFilter.md)
 - [SearchCustomersInfoRequest](docs/Model/SearchCustomersInfoRequest.md)
 - [SearchCustomersInfoResponse](docs/Model/SearchCustomersInfoResponse.md)
 - [SearchCustomersInfoResponseMeta](docs/Model/SearchCustomersInfoResponseMeta.md)
 - [SearchPreferencesFilter](docs/Model/SearchPreferencesFilter.md)
 - [SearchPreferencesRequest](docs/Model/SearchPreferencesRequest.md)
 - [SearchPreferencesResponse](docs/Model/SearchPreferencesResponse.md)
 - [SearchProductSubscribesRequest](docs/Model/SearchProductSubscribesRequest.md)
 - [SearchProductSubscribesResponse](docs/Model/SearchProductSubscribesResponse.md)


## Documentation For Authorization

All endpoints do not require authorization.

## Author

mail@greensight.ru

