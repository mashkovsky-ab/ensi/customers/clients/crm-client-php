<?php

namespace Ensi\CrmClient;

class CrmClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\CrmClient\Api\CustomerFavoritesApi',
        '\Ensi\CrmClient\Api\ProductSubscribesApi',
        '\Ensi\CrmClient\Api\CustomersInfoApi',
        '\Ensi\CrmClient\Api\PreferencesApi',
        '\Ensi\CrmClient\Api\BonusOperationsApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\CrmClient\Dto\SearchBonusOperationsRequest',
        '\Ensi\CrmClient\Dto\RequestBodyPagination',
        '\Ensi\CrmClient\Dto\ClearCustomerFavoritesRequest',
        '\Ensi\CrmClient\Dto\SearchCustomersInfoResponseMeta',
        '\Ensi\CrmClient\Dto\CustomerInfoFillableProperties',
        '\Ensi\CrmClient\Dto\ProductSubscribeForCreate',
        '\Ensi\CrmClient\Dto\Error',
        '\Ensi\CrmClient\Dto\PreferenceFillableProperties',
        '\Ensi\CrmClient\Dto\CustomerInfoReadonlyProperties',
        '\Ensi\CrmClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\CrmClient\Dto\CustomerFavoriteResponse',
        '\Ensi\CrmClient\Dto\PaginationTypeEnum',
        '\Ensi\CrmClient\Dto\CustomerInfoResponse',
        '\Ensi\CrmClient\Dto\CustomerFavoriteReadonlyProperties',
        '\Ensi\CrmClient\Dto\BonusOperationForPatch',
        '\Ensi\CrmClient\Dto\File',
        '\Ensi\CrmClient\Dto\EmptyDataResponse',
        '\Ensi\CrmClient\Dto\PreferenceReadonlyProperties',
        '\Ensi\CrmClient\Dto\BonusOperationReadonlyProperties',
        '\Ensi\CrmClient\Dto\BonusOperationResponse',
        '\Ensi\CrmClient\Dto\SearchPreferencesFilter',
        '\Ensi\CrmClient\Dto\BonusOperationForCreate',
        '\Ensi\CrmClient\Dto\Preference',
        '\Ensi\CrmClient\Dto\ProductSubscribeResponse',
        '\Ensi\CrmClient\Dto\ResponseBodyPagination',
        '\Ensi\CrmClient\Dto\SearchProductSubscribesRequest',
        '\Ensi\CrmClient\Dto\SearchCustomersInfoResponse',
        '\Ensi\CrmClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\CrmClient\Dto\ClearProductSubscribesRequest',
        '\Ensi\CrmClient\Dto\SearchCustomersInfoRequest',
        '\Ensi\CrmClient\Dto\CustomerStatusEnum',
        '\Ensi\CrmClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\CrmClient\Dto\PaginationTypeOffsetEnum',
        '\Ensi\CrmClient\Dto\CustomerInfoForCreate',
        '\Ensi\CrmClient\Dto\ErrorResponse',
        '\Ensi\CrmClient\Dto\CustomerInfo',
        '\Ensi\CrmClient\Dto\CustomerInfoForPatch',
        '\Ensi\CrmClient\Dto\SearchProductSubscribesResponse',
        '\Ensi\CrmClient\Dto\BonusOperationFillableProperties',
        '\Ensi\CrmClient\Dto\DeleteProductFromCustomerFavoritesRequest',
        '\Ensi\CrmClient\Dto\CustomerFavoriteFillableProperties',
        '\Ensi\CrmClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\CrmClient\Dto\RequestBodyCursorPagination',
        '\Ensi\CrmClient\Dto\ProductSubscribeReadonlyProperties',
        '\Ensi\CrmClient\Dto\SearchPreferencesRequest',
        '\Ensi\CrmClient\Dto\ModelInterface',
        '\Ensi\CrmClient\Dto\DeleteProductSubscribesRequest',
        '\Ensi\CrmClient\Dto\CustomerFavorite',
        '\Ensi\CrmClient\Dto\CustomerGenderEnum',
        '\Ensi\CrmClient\Dto\CustomerFavoriteForCreate',
        '\Ensi\CrmClient\Dto\SearchPreferencesResponse',
        '\Ensi\CrmClient\Dto\ProductSubscribe',
        '\Ensi\CrmClient\Dto\SearchCustomerFavoritesRequest',
        '\Ensi\CrmClient\Dto\ProductSubscribeFillableProperties',
        '\Ensi\CrmClient\Dto\SearchCustomersInfoFilter',
        '\Ensi\CrmClient\Dto\SearchCustomerFavoritesResponse',
        '\Ensi\CrmClient\Dto\SearchBonusOperationsResponse',
        '\Ensi\CrmClient\Dto\BonusOperation',
    ];

    /** @var string */
    public static $configuration = '\Ensi\CrmClient\Configuration';
}
